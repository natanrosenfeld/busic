package tltg.busic;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    public void startButtonOnClick(View v){
        Toast.makeText(MainActivity.this, "Start", Toast.LENGTH_SHORT).show();
    }
    public void aboutButtonOnClick(View v){
        Intent i = new Intent (MainActivity.this, AboutActivity.class);
        startActivity(i);
    }
    public void exitButtonOnClick(View v){
        new AlertDialog.Builder(this)
                .setTitle("Exit Busic?")
                .setMessage("Are you sure you want to exit Busic?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        System.exit(0);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .setIcon(android.R.drawable.ic_menu_help)
                .show();
    }

}
